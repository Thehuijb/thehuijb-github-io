export default function myIcon ({ html, state = {} }) {
    const { attrs = {} } = state;
    const {url = '', icon = ''} = attrs;
    if (url && icon) {
        return html`
        <a href="${url}"><i class="svg-icon ${icon}"></i></a>
        `
    }
    return html`
    <a href="/"><i class="svg-icon discord"></i></a>
    `
  }